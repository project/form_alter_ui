<?php

/**
 * Settings form for the Form Alter UI module itself.
 */
function form_alter_ui_settings(&$form_state, $theme = NULL) {
  $form = array();

  // Checkbox - capture log of viewed forms
  $form['form_alter_ui_enable'] = array(
    '#title' => t('Enable capture of viewed forms'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('form_alter_ui_enable', FALSE),
    '#description' => t('When this option is enabled, the module will record all forms seen.'),
  );

  // TODO: user selector - users to capture forms for

  // List of recently viewed forms
  $header = array(
    array('data' => t('Form ID'), 'field' => 'form_id'),
    array('data' => t('Last Accessed'), 'field' => 'timestamp', 'sort' => 'desc'),
    t('Operations'),
  );

  $sql = 'SELECT form_id, timestamp, location FROM {form_alter_ui_log}';
  $tablesort = tablesort_sql($header);

  $result = pager_query($sql . $tablesort, 10);

  $rows = array();
  while ($row = db_fetch_object($result)) {
    $rows[] = array(
      $row->form_id,
      format_date($row->timestamp) .' '. l('view', $row->location),
      l('modify', 'admin/settings/form_alter/alter/'. $row->form_id),
    );
  }

  $form['recent'] = array(
    '#type' => 'markup',
    '#value' => theme('table', $header, $rows) . theme('pager', NULL, 10, 0),
  );

  // Save it all in system variables (for now)
  return system_settings_form($form);
}

/**
 * The form used to modify an existing (subject) form.
 */
function form_alter_ui_alter_form(&$form_state, $form_id = NULL) {
  // Load the saved form information
  $saved = _form_alter_ui_load_saved_form($form_id);
  if ($saved == FALSE) {
    return;
  }

  // Prepare for processing of the subject form
  $var_name = 'form_alter_ui_modify_'. $form_id;
  $default_values = variable_get($var_name, array());

  // Form-level alterations
  $form = array(
    '#tree' => TRUE,
    $var_name => array(),
  );
  $form[$var_name]['form'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
    '#title' => t('Alter Entire Form'),
    '#description' => t('In this section, you can apply changes to the entire form.'),
  );

  $form[$var_name]['form']['preamble'] = array(
    // t('This is an example of the form that will be modified.') .
    '#value' => '<pre>$form = '. htmlspecialchars(print_r($saved->form, TRUE)) .'</pre>',
  );

  $form[$var_name]['form']['php'] = array(
    '#type' => 'textarea',
    '#title' => 'Custom PHP',
    '#description' => t('You may post PHP code that will be executed for form "%form_id". You should NOT include &lt;?php ?&gt; tags. At the time of execution, both $form and $form_state are defined. This is executed BEFORE any field-level modifications below.', array('%form_id' => $form_id)),
    '#default_value' => isset($default_values['form']['php']) ? $default_values['form']['php'] : '',
    '#rows' => 10,
  );

  // Element-level alterations
  foreach ($saved->form as $key => $element) {
    if ($key[0] == '#') {
      continue;
    }

    $form[$var_name][$key] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
      '#title' => t('Alter @key', array('@key' => $key)),
    );

    $form_element = &$form[$var_name][$key];
    $element_defaults = $default_values[$key];

    $form_element['preamble'] = array(
      '#value' => '<pre>$element = '. htmlspecialchars(print_r($element, TRUE)) .'</pre>',
    );

    foreach (_form_alter_ui_get_elements() as $akey=> $aelement) {
      $adefault = isset($element_defaults[$akey]) ? $element_defaults[$akey] : form_alter_ui_DEFAULT_VALUE;
      $form_element[$akey] = $aelement + array(
        '#default_value' => $adefault,
      );
    }

    $form_element['php'] = array(
      '#type' => 'textarea',
      '#title' => 'Custom PHP',
      '#description' => t('You may post PHP code that will be executed for form element "%key". You should NOT include &lt;?php ?&gt; tags. You may modify $element.', array('%key' => $key)),
      '#default_value' => $element_defaults['php'],
      '#rows' => 5,
    );
  }

/*
    // TODO: some function for "adding modifications" to fields
    // EG: ADD: setvalue + description + "new description"
    // TODO: include token support
    // TODO: include PHP support


     //ADDED TO ALL ELEMENTS
     //#description
     //#required
     //#tree
     //#post
     //#parents
     //#attributes
     //
     //ALLOWED IN ALL ELEMENTS
     //#type
     //#access
     //#process
     //#theme
     //#prefix
     //#suffix
     //#title
     //#weight
     //#default_value

      //// Value
      //$form
      //$form[$var_name][$key]['enabled'] = array(
      //  '#title' => t('Enable alteration of %key', array('%key' => $key)),
      //  '#type' => 'checkbox',
      //  '#default_value' => $default_value[$key]['enabled'],
      //  // '#description' => t('When this option is enabled, the module will record the last 100 forms seen.'),
      //);
  }
*/


  // TODO: display field-level alterations

  // Retrieve the form as saved at the time
  // $form = call_user_func_array('drupal_retrieve_form', $saved->form['#parameters']);
  // $saved_form = $saved->form;
  // dsm("Saved form:");
  // dsm($saved->form);
  drupal_set_title(t('Modify Form %form_id', array('%form_id' => $form_id)));
  return system_settings_form($form);

}



/**
 * Form Alter settings export form.
 */
function form_alter_ui_export_form(&$form_state, $theme = NULL) {
  $form = array();
  // ...
  return $form;
}

/**
 * Form Alter settings import form.
 */
function form_alter_ui_import_form(&$form_state) {
  $form = array();
  // ...
  return $form;
}



function _form_alter_ui_load_saved_form($form_id) {
  // Load the saved form information to get the arguments
  $result = db_query("SELECT * FROM {form_alter_ui_log} WHERE form_id = '%s'", $form_id);
  if ($row = db_fetch_object($result)) {
    $row->form = unserialize($row->form);
  }
  return $row;
}
